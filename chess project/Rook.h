#pragma once

#include "Piece.h"


class Rook : public Piece {
public:
	/// <summary>
	///	Create a rook piece
	/// </summary>
	/// <param name="white">is the piece white</param>
	/// <param name="pos">the position of the piece on the board</param>
	Rook(const bool white, const Pos pos);
	
	std::vector<Pos> getAllMoves() const override;

	std::vector<Pos> getAllValidMoves() const override;

protected:
	/// <summary>
	/// function will return all the positions that are before/after the piece
	/// for example: if the piece is in [5][4]
	///  the positions before the piece are: [5][3], [5][2], [5][1], [5][0] 
	///  the positions after  the piece are: [5][5], [5][6], [5][7]
	/// </summary>
	/// <param name="beforePiece">what will the function return? the positions before or after the piece</param>
	/// <returns>if beforePiece is true then all the positions before the piece, else all the positons after it</returns>
	std::vector<Pos> getAllPosInRow(bool beforePiece) const;


	/// <summary>
	/// function will return all the positions that are below/above the piece
	/// for example: if the piece is in [5][4]
	///  the positions below the piece are: [4][4], [3][4], [2][4], [1][4], [0][4]
	///  the positions above the piece are: [6][4], [7][4]
	/// </summary>
	/// <param name="belowPiece">what will the function return? the positions above or below the piece</param>
	/// <returns>if belowPiece is true then all the positions below the piece, else all the positons above it</returns>
	std::vector<Pos> getAllPosInColumn(bool belowPiece) const;
};