#pragma once

#include <optional>
#include <vector>

#include "Piece.h"
#include "Pos.h"


class Piece;

typedef std::unique_ptr<Piece> piecePtr; // unique pointer for Piece

typedef std::optional<std::reference_wrapper<Piece>> optionalPiece; // because std::optional<Piece&> doesn't compile
/// <summary>
/// The chess board
/// 
/// contains the board 
/// </summary>
class Board {
private:
	static const int SIZE = 8;

	piecePtr _positions[SIZE][SIZE];
	bool _whitesTurn;

public:

	/// <summary>
	///	Default constructor: Creates the board (standard chess board), sets whitesTurn to true
	/// </summary>
	Board();

	/// <summary>
	/// returns the piece in the wanted position 
	/// </summary>
	/// <param name="pos">the position of the piece</param>
	/// <returns>the piece in pos</returns>
	optionalPiece getPiece(const Pos pos);

	/// <summary>
	/// Inserts the piece into the vector (according to pos)
	/// 
	/// If there is already a piece there it will be deleted
	/// </summary>
	/// <param name="piece">the piece we want to insert into the board</param>
	void insertPiece(piecePtr piece);

	/// <summary>
	/// Delete a piece from the board by position
	/// </summary>
	/// <param name="pos">The position of the piece</param>
	/// <returns>true if"f there was a piece in that position (and it was deleted)</returns>
	bool deletePiece(Pos pos);
};
