#include "Rook.h"

std::vector<Pos> Rook::getAllMoves() const
{
	std::vector<Pos> allMoves;

	std::vector<Pos> posBefore = this->getAllPosInRow(true);
	std::vector<Pos> posAfter = this->getAllPosInRow(false);
	std::vector<Pos> posBelow = this->getAllPosInColumn(true);
	std::vector<Pos> posAbove = this->getAllPosInColumn(false);

	// add all moves rook can make
	for (Pos before : posBefore)
		allMoves.push_back(before);
	for (Pos after : posAfter)
		allMoves.push_back(after);

	for (Pos below : posBelow)
		allMoves.push_back(below);
	for (Pos above : posAbove)
		allMoves.push_back(above);

	return allMoves;
}

std::vector<Pos> Rook::getAllValidMoves() const
{
	enum {BEFORE, AFTER, BELOW, ABOVE};
	std::vector<Pos> allValidMoves;

	// first we get all the moves the piece can make but we devide them into 4 parts (below, above, before and after the piece)
	std::vector< std::vector<Pos> > allPos;
	allPos.push_back(this->getAllPosInRow(true));
	allPos.push_back(this->getAllPosInRow(false));
	allPos.push_back(this->getAllPosInColumn(true));
	allPos.push_back(this->getAllPosInColumn(false));

	// second thing we do we reverse the order of the below and before positions (so we can use a simpler code)
	std::reverse(allPos[BEFORE].begin(), allPos[BEFORE].end());
	std::reverse(allPos[BELOW].begin(), allPos[BELOW].end());
	
	// now, in each part we'll only keep the valid moves
	for (std::vector<Pos> partOfAllPos : allPos)
	{
		for (Pos pos : partOfAllPos)
		{
			// TODO: check for check/mate

			//	If there is a piece, and it is the same color as this
			optionalPiece pieceAtValidPosMaybe = this->_board->getPiece(pos);
			if (pieceAtValidPosMaybe.has_value()) {
				Piece& pieceAtNewPos = pieceAtValidPosMaybe.value();
				if (pieceAtNewPos.isWhite() == this->_white) {
					break;
				}
				else // if theres a piece but its a diffrent color, allow that move then break
				{
					allValidMoves.push_back(pos);
					break;
				}
			}

			// if did'nt break, push the value to allValidMoves
			allValidMoves.push_back(pos);
		}
	}

	return allValidMoves;
}
	

std::vector<Pos> Rook::getAllPosInRow(bool beforePiece) const
{
	const int MIN_X = 0;
	const int MAX_X = 8;

	int y = this->_pos.getY();
	std::vector<Pos> posAfterPiece;
	std::vector<Pos> posBeforePiece;

	// add all indexes from [y][0] to [y][7]
	for (int x = MIN_X; x < MAX_X; x++)
	{
		if (x < this->_pos.getX()) // while x is lower then the x of the piece, enter it to the vector of positions before the piece
			posBeforePiece.push_back(Pos(x, y));
		else if (x > this->_pos.getX()) // adds all the positions with x that is bigger then the x of the piece to the vector of positions after the piece
			posAfterPiece.push_back(Pos(y, x));
	}

	if (beforePiece) // if true, return all positions before the piece
		return posBeforePiece;

	return posAfterPiece; // if not then return all the positions after the piece

}

std::vector<Pos> Rook::getAllPosInColumn(bool belowPiece) const
{
	const int MIN_Y = 0;
	const int MAX_Y = 8;

	int x = this->_pos.getX();
	std::vector<Pos> posAbovePiece;
	std::vector<Pos> posBelowPiece;

	// add all indexes from [0][x] to [7][x]
	for (int y = MIN_Y; y < MAX_Y; y++)
	{
		if (y < this->_pos.getY()) // while y is lower then the y of the piece, enter it to the vector of positions below the piece
			posBelowPiece.push_back(Pos(x, y));
		else if (y > this->_pos.getY()) // adds all the positions with y that is bigger then the y of the piece to the vector of positions above the piece
			posAbovePiece.push_back(Pos(y, x));
	}

	if (belowPiece) // if true, return all positions below the piece
		return posBelowPiece;

	return posAbovePiece; // if not then return all the positions above the piece
}
