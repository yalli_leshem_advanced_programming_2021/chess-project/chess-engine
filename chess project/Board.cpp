#include "Board.h"

Board::Board() : _positions{}, _whitesTurn(true) { }

optionalPiece Board::getPiece(const Pos pos)
{
	const piecePtr& pPiece = this->_positions[pos.getY()][pos.getX()];
	
	//	If pPiece is null
	if (!pPiece) {
		return optionalPiece();
	}
	
	return optionalPiece(*pPiece);
}

void Board::insertPiece(piecePtr piece) {
	Pos p = piece->getPos();
	this->_positions[p.getY()][p.getX()] = std::move(piece);
}

bool Board::deletePiece(Pos pos)
{
	piecePtr& pPiece = this->_positions[pos.getY()][pos.getX()];
	if (!pPiece) {
		return false;
	}
	else {
		pPiece = nullptr;	//	deletes the piece (because pPiece references the pointer to the piece)
		return true;
	}
}
