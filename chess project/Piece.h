#pragma once

#include <vector>

#include "Pos.h"
#include "Board.h"

class Board;

/// <summary>
/// master class of all the pieces (subclasses will be: | Queen | King | Rook | Bishop | Knight | Pawn |)
/// </summary>
class Piece {
protected:
	Board* _board; 
	bool _white;
	Pos _pos;

public:

	/// <summary>
	///	constructor: adds to the board the new piece
	/// 
	/// Does not assign the piece to a board. make sure to call insertPiece before using this object.
	/// </summary>
	/// <param name="white">is the piece white</param>
	/// <param name="pos">the position of the piece on the board</param>
	Piece(const bool white, const Pos pos);

	//	---------------------------
	//		    Getters
	//	---------------------------
	
	Pos getPos() const;
	bool isWhite() const;

	//	---------------------------
	//			Utility
	//	---------------------------

	//// <summary>
	/// Returns a vector of all the positions the piece can go to
	/// each piece moves in differently so this is virtual
	/// </summary>
	/// <returns></returns>
	virtual std::vector<Pos> getAllMoves() const = 0;

	/// <summary>
	/// Returns a vector of all the valid positions the piece can go to
	/// after checking all the moves the piece can make (valid and invald), return only the valid moves (diffrent for each piece)
	/// </summary>
	/// <returns></returns>
	virtual std::vector<Pos> getAllValidMoves() const = 0;

	/// <summary>
	/// checks if the move is valid
	/// checks if the wanted position is valid 
	/// (basically checks if the position is in the vector of all valid moves)
	/// </summary>
	/// <param name="newPos">the targeted position</param>
	/// <returns>true - if move is indeed valid | false - if not</returns>
	bool isMoveValid(const Pos newPos) const;

	/// <summary>
	/// Moves the piece to another position (and creates an empty piece in the old position)
	/// will only work if move is valid
	/// <param name="newPos">the targeted position</param>
	/// </summary>
	void movePiece(const Pos newPos);


protected:
	friend class Board;
};