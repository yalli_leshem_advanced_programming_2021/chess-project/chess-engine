#include "Piece.h"

Piece::Piece(const bool white, const Pos pos) : _white(white), _pos(pos) {}

Pos Piece::getPos() const
{
	return this->_pos;
}

bool Piece::isWhite() const
{
	return this->_white;
}

bool Piece::isMoveValid(const Pos newPos) const
{
	bool validMove = false;
	std::vector<Pos> AllValidMoves = this->getAllValidMoves();

	for (Pos validPos : AllValidMoves)
	{
		if (validPos == newPos)
			validMove = true;
	}

	return validMove;
}

